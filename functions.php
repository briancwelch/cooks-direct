<?php
/**
 * Theme Name:        Cooks Direct Child Theme
 * Theme URI:         http://briancwelch.com
 * Description:       Cooks Direct blog child theme.  Built using the Maera WordPress framework and the Maera Bootstrap Shell.
 * Version:           1.0.0
 * Author:            Brian Welch
 * Author URI:        http://briancwelch.com
 * Requires at least: 4.0
 * Tested up to:      4.1
 * License:           MIT
 *
 * Text Domain:       cooks-direct
 * Domain Path:       /lang/
 *
 * @package           Cooks Direct
 * @category          Theme
 * @author            Brian Welch
 *
 */


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'COOKS_DIRECT_VERSION', '1.0.0' );

if ( ! class_exists( 'Cooks_Direct' ) ) {

	/**
	* The Cooks Direct Core Class
	*/
	class Cooks_Direct {

		private static $instance;

		/**
		 * Singleton
		 * @return self::$instance
		 */
		public static function get_instance() {

			if ( null == self::$instance ) {
				self::$instance = new self;
			}

			return self::$instance;

		}


		/**
		 * Class constructor
		 */
		public function __construct() {

			// Define some globals to make it easier on other developers.
			if ( ! defined( 'COOKS_DIRECT_FILE' ) ) { define( 'COOKS_DIRECT_FILE', __FILE__ ); }
			if ( ! defined( 'COOKS_DIRECT_PATH' ) ) { define( 'COOKS_DIRECT_PATH', dirname( __FILE__ ) ); }
			if ( ! defined( 'COOKS_DIRECT_URL' ) ) { define( 'COOKS_DIRECT_URL', get_stylesheet_directory_uri( __FILE__ ) ); }

			$this->requires();

			// Add actions.
			add_action( 'plugins_loaded', array( $this, 'cd_translations' ) );       // Load Translations.
			add_action( 'wp_enqueue_scripts', array( $this, 'cd_scripts' ), 99 );    // Load scripts and stylesheets.  Set Index to 99 so it loads last.
			add_action( 'maera/content/before', array( $this, 'cd_coupon_twig' ) );  // Inject our coupon button twig.
			add_action( 'maera/content/before', array( $this, 'cd_pre_content' ) );  // Inject our custom header.
			// add_action( 'maera/footer/content', array( $this, 'cd_footer_twig' ) );  // Inject our social icons into the footer.
			add_action( 'edit_category_form_pre', array( $this, 'cd_cat_form_pre' ) );
			add_action( 'edit_category_form', array( $this, 'cd_cat_form' ) );
			add_action( 'edit_category', array( $this, 'cd_cat_form_save' ) );

			// Add filters.
			// NULL. No filters currently in use.
		}


		/**
		 * Include any required files.
		 * @since  1.0.0
		 * @return [type]
		 */
		function requires() {
			// NULL.  There are currently no extra required files.
			// If you need to require or include any additional files, enter their requirements here.
			// Ex: require_once( COOKS_DIRECT_PATH . '/folder/myfile.php');
		}


		/**
		 * Load theme textdomain.
		 * @since 1.0.0
		 * @return [type] [description]
		 */
		function cd_translations() {

			load_child_theme_textdomain(
				'cooks-direct',
				dirname( plugin_basename( __FILE__ ) ) . '/lang/'
			);
			// This is required for localization and translations.  English is included by default.
			// Any .po/.mo file may be generated and placed in the /lang/ folder.
		}


		/**
		 * Load scripts if necessary.
		 * @return [type] [description]
		 */
		function cd_scripts() {

			// Load our custom stylesheet.  Check /assets/css/style.css for more information.
			// Enable this only if you want to override style.css in the base directory. You can then make your modifications from there.

			// wp_register_style( 'cooks-direct', COOKS_DIRECT_URL . '/assets/css/style.css', null, time(), 'all' );
			// wp_enqueue_style( 'cooks-direct' );

			// Load FontAwesome from the CDN.
			wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', null, null, 'all' );

			// Load jQuery UI from the CDN.
			wp_register_script( 'jquery_ui', '//code.jquery.com/ui/1.11.2/jquery-ui.js', array('jquery'), time(), false );
			wp_enqueue_script( 'jquery_ui' );

			// Load our own custom Javascript file.
			wp_register_script( 'cd_js', COOKS_DIRECT_URL . '/assets/js/cd.js', array('jquery'), time(), false );
			wp_enqueue_script( 'cd_js' );

		}


		/**
		 * Modify the pre-content with our custom twig.
		 * Please check the /views folder for more info.
		 * @return [type] [description]
		 */
		function cd_pre_content() {

			if ( ! is_front_page() ){
				// Do not remove this conditional, as it will render our custom content header on the front page.
				$context = Maera()->template->context();
				Maera()->template->render( 'pre_content.twig', $context );
			}

		}


		/**
		 * Modify the pre-content with our custom twig.
		 * Please check the /views folder for more info.
		 * @return [type] [description]
		 */
		function cd_coupon_twig() {

			if ( ! is_front_page() ){
				// Do we want the coupons button on every page?  If so, remove the conditional.
				$context = Maera()->template->context();
				Maera()->template->render( 'coupons.twig', $context );
			}

		}


		/**
		 * Modify the footer content.
		 * Please check the /views folder for more info.
		 * @return [type] [description]
		 */
		function cd_footer_twig() {

			$context = Maera()->template->context();

			$context['facebook_icon'] = COOKS_DIRECT_URL . '/assets/images/facebook.png';
			$context['twitter_icon'] = COOKS_DIRECT_URL . '/assets/images/twitter.png';
			$context['googleplus_icon'] = COOKS_DIRECT_URL . '/assets/images/googleplus.png';
			$context['youtube_icon'] = COOKS_DIRECT_URL . '/assets/images/youtube.png';

			Maera()->template->render( 'footer-icons.twig', $context );

		}


		/**
		 * [cd_cat_form_pre description]
		 * @param  [type] $p [description]
		 * @return [type]    [description]
		 */
		function cd_cat_form_pre($p){
			ob_start();
		}


		/**
		 * [cd_cat_form description]
		 * @param  [type] $cat [description]
		 * @return [type]      [description]
		 */
		function cd_cat_form($cat){
			$form = ob_get_contents();
			ob_end_clean();
			$form = str_replace( '<form name="edittag"','<form name="editcat" enctype="multipart/form-data" ', $form );

			$attachments = self::get_cat_images( $cat->term_id );
			$image_list = '';
			foreach ( $attachments as $att ){
				$image_list .= '<li>' . wp_get_attachment_image( $att->ID, $size = 'thumbnail', $icon = false ) . "<input type=\"checkbox\" class=\"remove_cat_image\" name=\"remove_cat_image[$att->ID]\" title=\"remove image\" /></li>";
			}

			$form = str_replace('<table class="form-table">'
				,'<style>.category-thumbnail li {float:left;position:relative;} .category-thumbnail li .remove_cat_image {position:absolute;top:2px; right:2px; z-index:2;} </style>'
				.'<ul class="category-thumbnail clearfix">' . $image_list . '</ul>'
				.'<table class="form-table"><tr><th>'. __( 'Set category image' ) . '</th><td><input type="hidden" name="att_cat_id" value="'. $cat->cat_ID .'" /><input type="file" name="cat_image_0" value="Imagen"></td></tr>'
				,$form
			);
			echo $form;
		}


		/**
		 * [cd_cat_form_save description]
		 * @param  [type] $cid [description]
		 * @return [type]      [description]
		 */
		function cd_cat_form_save($cid){

			$cat_id = ( int )$_POST['tag_ID'];
			$catinfo = get_category( $cat_id );
			if ( ! empty( $_POST['remove_cat_image'] ) ){
				$cat_images = self::get_cat_images( $cat_id );
				foreach ( $cat_images as $cat_image ) {
					if ( isset( $_POST['remove_cat_image'][$cat_image->ID] ) ){
						wp_delete_attachment( $cat_image->ID );
					}
				}
			}

			foreach ( array(0) as $i ){  // future posibility to upload multiple images
				if ( 0 != empty( $_FILES['cat_image_'.$i] ) || $_FILES['cat_image_'.$i]['error'] ){
					continue;
				}
				$FILE = $_FILES['cat_image_'.$i];
				$overrides = array( 'action' => 'wp_handle_upload', 'test_form' => false );
				$file = wp_handle_upload( $FILE, $overrides );
				if ( $file ){
					if ( isset( $file['error'] ) ) {
						die( $file['error'] );
					}

					$url = $file['url'];
					$type = $file['type'];
					$file = $file['file'];
					$filename = basename( $file );

					// Construct the object array
					$object = array(
						'post_title' => '['.$catinfo->name.']'.$filename,
						'post_content' => var_export( $_POST,true ),//$url,
						'post_mime_type' => $type,
						'guid' => $url,
					);

					// Save the data
					$id = wp_insert_attachment( $object, $file );

					wp_update_attachment_metadata( $id, wp_generate_attachment_metadata( $id, $file ) );

					do_action( 'wp_create_file_in_uploads', $file, $id ); // For replication
					if ( add_post_meta( $id, 'category_id', $cat_id, true ) ) {
						update_post_meta( $id, 'category_id', $cat_id, true );
					}
				}
			}

		}


		/**
		 * [get_cat_images description]
		 * @param  [type]  $cid   [description]
		 * @param  integer $limit [description]
		 * @return [type]         [description]
		 */
		function get_cat_images( $cid, $limit = -1 ){
			return get_posts( 'numberposts='.$limit.'&post_type=attachment&meta_key=category_id&meta_value='.$cid );
		}


		/**
		 * [cat_image description]
		 * @param  integer $cid  [description]
		 * @param  boolean $echo [description]
		 * @return [type]        [description]
		 */
		public function cat_image( $cid = 0, $echo = true ){
			if ( ! $cid ){
				$cid = get_the_ID();
			}
			$return = '';
			$attachments = self::get_cat_images( $cid, 1 );
			foreach ( $attachments as $att ){
				$return .= wp_get_attachment_image( $att->ID, $size = 'thumbnail', $icon = false );
			}

			return $return;
		}

		// End Methods
	} // End Class
} // End if


// Instantiate the main class
// This loads the main class and all of the methods above.
Cooks_Direct::get_instance();


/**
 * Cooks Direct - Social Widget Class
 * This will create a widget with links to Cooks Direct social pages that can be placed in any widget area.
 * Any changes made to the twig file will also change how the social icons are displayed in the footer.
 * Check the /views folder for more info.
 */
if ( ! class_exists( 'Cooks_Direct_Social_widget' ) ) {
	class Cooks_Direct_Social_widget extends WP_Widget {

		function __construct() {

			parent::__construct(
				'cd_social_widget',
				__( 'Cooks Direct Social Widget', 'wpb_widget_domain' ),
				array( 'description' => __( 'Links to Cooks Direct social pages', 'wpb_widget_domain' ), )
			);

		}


		public function widget( $args, $instance ) {
			$title = apply_filters( 'widget_title', $instance['title'] );

			echo _e( $args['before_widget'] );

			if ( ! empty( $title ) ){
				echo _e( $args['before_title'] . $title . $args['after_title'] );
			}

			$context['facebook_icon'] = COOKS_DIRECT_URL . '/assets/images/facebook.png';
			$context['twitter_icon'] = COOKS_DIRECT_URL . '/assets/images/twitter.png';
			$context['googleplus_icon'] = COOKS_DIRECT_URL . '/assets/images/googleplus.png';
			$context['youtube_icon'] = COOKS_DIRECT_URL . '/assets/images/youtube.png';

			Maera()->template->render( 'footer-icons.twig', $context );
			echo _e( $args['after_widget'] );
		}

		public function form( $instance ) {
			if ( isset( $instance[ 'title' ] ) ) {
				$title = $instance[ 'title' ];
			} else {
				$title = __( 'New title', 'wpb_widget_domain' );
			}
		?>
		<p>
			<label for="<?php _e( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php _e( $this->get_field_id( 'title' ) ); ?>" name="<?php _e( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php
		}

		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			return $instance;
		}

		// End Methods
	} // End Class
} // End if


// Register and load the social widget
function cd_load_social_widget() {
	register_widget( 'Cooks_Direct_Social_widget' );
}
add_action( 'widgets_init', 'cd_load_social_widget' );



/**
 * [cat_image description]
 * @param  integer $cid  [description]
 * @param  boolean $echo [description]
 * @return [type]        [description]
 */
function cat_image( $cid = 0, $echo = true ){
	if ( ! $cid ){
		$cid = get_the_ID();
	}
	$return = '';
	$attachments = Cooks_Direct::get_cat_images( $cid, 1 );
	foreach ( $attachments as $att ){
		$return .= wp_get_attachment_image( $att->ID, $size = 'full', $icon = false );
	}

	return $return;
}

// EOF
