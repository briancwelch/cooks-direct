Cook's Direct - Maera Child Theme
====================

# Description #
This is a child theme built for Cook's Direct using the [Maera](http://maera.io/) framework and [Kirki](http://kirki.org/) customizer.
Requires WordPress 4.0+ and [Maera](http://maera.io/)


# Frequently Asked Questions #
**Q: I want to change stuff, what files do I edit?**
*You might not need to edit anything at all!  Check out the theme customizer first by going to the WordPress admin dashboard, then going to Appearance > Customize.  DO NOT edit style.css in the root directory.  Instead, open up functions.php and uncomment the loading of our custom stylesheet, and then make your edits in the /assets/css/style.css file.*

**Q: What is the /views folder and what are all the .twig files?**
*The Maera framework uses Timber/Twig, which is a PHP templating engine, similar to Smarty.  The /views folder contains files that we want to override with our own content.  DO NOT edit anything in the /maera/ theme directory or the /maera-bootstrap/ plugins directory.  Your changes will be overridden as they are updated often.*


# Changelog #
## 1.0.0 ##
Initial Version


# Translations #
* English: Default - Always included.


# Credits #
* @briancwelch - [Web Developer](https://briancwelch.com).


# Additional Info #
No additional information is available at this time.
